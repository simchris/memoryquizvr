﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public static SceneHandler instance = null;

    public List<string> scenes;

    private int sceneIndex;

    //make sure that we only have a single instance of the game manager
    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        sceneIndex = 0;    
    }

    // Update is called once per frame
    void Update()
    {
        CheckForInput();
    }

    private void CheckForInput()
    {
        if (Input.GetKeyUp(KeyCode.Return))
            if (sceneIndex < scenes.Count)
            {
                sceneIndex++;
                Debug.Log("The index now is: " + sceneIndex);
                SceneManager.LoadScene(scenes[sceneIndex]);
            }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown("1"))
        {
            SceneManager.LoadScene(scenes[0]);
            sceneIndex = 0;
        }
        else if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey("2"))
        {
            SceneManager.LoadScene(scenes[1]);
            sceneIndex = 1;
        }
        else if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyUp("3"))
        {
            SceneManager.LoadScene(scenes[2]);
            sceneIndex = 2;
        }
        else if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown("4"))
        {
            SceneManager.LoadScene(scenes[3]);
            sceneIndex = 3;
        }
    }
}
