﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    private bool waitActive = false;
    private bool audioStarted = false;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = this.gameObject.GetComponent<AudioSource>();
        
        if (!waitActive)
        {
            StartCoroutine(Wait());
        }
    }

    IEnumerator Wait()
    {
        waitActive = true;
        yield return new WaitForSeconds(1.5f);
        audioSource.Play();
        audioStarted = true;
        waitActive = false;
    }

}
